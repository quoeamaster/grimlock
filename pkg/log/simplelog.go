// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// grimlock - a log framework
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package log

import (
	"fmt"
	"time"
)

type simpleLog struct {

	// name of the logger instance.
	name string
	// thresholdLevel is the mini level this logger could service.
	thresholdLevel int

	// appender(s) ILogAppender
}

// Name returns the logger's name | ID.
func (s *simpleLog) Name() string {
	return s.name
}

// IsLoggable checks whether the provided logLevel is valid for the logging operation.
func (s *simpleLog) IsLoggable(logLevel int) bool {
	// TODO: return true for now
	return true
}

// Debug logs down the provided line argument if the logger is valid to log Debug logs.
func (s *simpleLog) Debug(line string, meta ...LogMeta) (isLogged bool) {
	return s.Log(line, 1, meta...)
}

// Error logs down the provided line argument if the logger is valid to log Error logs.
func (s *simpleLog) Error(line string, meta ...LogMeta) (isLogged bool) {
	return s.Log(line, 1, meta...)
}

// Log logs down the provided line argument if the logger is valid to do so.
func (s *simpleLog) Log(line string, logLevel int, meta ...LogMeta) (isLogged bool) {
	// TODO: now everything is ok...
	// TODO: meta is ignored...
	isLogged = s.IsLoggable(logLevel)
	if true == isLogged {
		fmt.Printf("[%v] %v\n", time.Now().Format(time.RFC3339Nano), line)
	}
	return
}
