// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// grimlock - a log framework
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package log

// ILog is an interface describing a logger instance.
type ILog interface {

	// Name returns the logger's name | ID.
	Name() string

	// IsLoggable checks whether the provided logLevel is valid for the logging operation.
	IsLoggable(logLevel int) bool

	// Debug logs down the provided line argument if the logger is valid to log Debug logs.
	Debug(line string, meta ...LogMeta) (isLogged bool)
	// Error logs down the provided line argument if the logger is valid to log Error logs.
	Error(line string, meta ...LogMeta) (isLogged bool)
	// Log logs down the provided line argument if the logger is valid to do so.
	Log(line string, logLevel int, meta ...LogMeta) (isLogged bool)
}

// LogMeta represents the meta data for a log line.
type LogMeta struct {
	moduleName string
	funcName   string
	optional   string
}
