// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// grimlock - a log framework
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"testing"
	"time"

	"gitlab.com/quoeamaster/grimlock/internal"
	"gitlab.com/quoeamaster/grimlock/pkg/log"
)

func Test_simplelog_00(t *testing.T) {
	line, _ := internal.UTestHeader("simplelog_test", "Test_simplelog_00", true)
	t.Log(line)

	_log := log.CreateLogger("whatever", "")
	if ok := _log.Log("hello world", 1); false == ok {
		t.Fatalf("expected simplelog to be able to log level 1, but failed")
	}

	// end
	line, _ = internal.UTestTrailer("simplelog_test", "Test_simplelog_00", false, time.Time{})
	t.Log(line)
}
